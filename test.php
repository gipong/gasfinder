﻿<?php
	$client = new SoapClient("http://vipmember.tmtd.cpc.com.tw/CPCSTN/STNWebService.asmx?WSDL");
	$results = $client->getCityStation();

	$array = $results->getCityStationResult->any;
	$obj = simplexml_load_string($array);

	$row = $obj->NewDataSet;
	$number = count($row->tbTable);
	//var_dump($row->tbTable[0]);
	$gas_list = array();
	
	for($i = 0; $i < $number; $i++) {
		$data = $row->tbTable[$i];
		if($data->縣市 == '台南市') {
			//echo '站名:'.$data->站名.' 經度:'.$data->經度.' 緯度:'.$data->緯度.'<br />';
			$gas_list["$data->站名"] = array();
			array_push($gas_list["$data->站名"], $data->經度);
			array_push($gas_list["$data->站名"], $data->緯度);
			array_push($gas_list["$data->站名"], $data->電話);
			array_push($gas_list["$data->站名"], $data->營業中);
			
		}
	}
	
	//var_dump($gas_list);
	
	//echo $_GET['lat'];
	echo json_encode($gas_list);